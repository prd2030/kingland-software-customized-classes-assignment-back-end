INSERT INTO users (name, email, created_at) VALUES
('Alice', 'alice@example.com', NOW()),
('Bob', 'bob@example.com', NOW()),
('Charlie', 'charlie@example.com', NOW()),
('David', 'david@example.com', NOW()),
('Eve', 'eve@example.com', NOW()),
('Frank', 'frank@example.com', NOW()),
('Grace', 'grace@example.com', NOW()),
('Hank', 'hank@example.com', NOW()),
('Ivy', 'ivy@example.com', NOW()),
('Jack', 'jack@example.com', NOW());
