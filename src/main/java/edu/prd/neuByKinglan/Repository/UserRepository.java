package edu.prd.neuByKinglan.Repository;


import edu.prd.neuByKinglan.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Long> {
}