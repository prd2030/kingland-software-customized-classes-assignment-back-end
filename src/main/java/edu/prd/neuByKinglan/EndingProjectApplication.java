package edu.prd.neuByKinglan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EndingProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(EndingProjectApplication.class, args);
    }

}
